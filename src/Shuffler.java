import java.util.Random;

public class Shuffler {
    public static void shuffle(int[] arr) {
        Random rnd = new Random(System.nanoTime());
        for (int i = 0; i < arr.length; i++) {
            swap(arr, i, rnd.nextInt(arr.length));
        }
    }

    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }
}




//graph = new int[][]{
//        {0, 3, 2, 5, 999, 999, 999, 999, 999, 1},
//        {3, 0, 4, 8, 5, 999, 999, 999, 999, 5},
//        {2, 4, 0, 7, 4, 5, 999, 999, 999, 7},
//        {5, 8, 7, 0, 4, 4, 7, 999, 9, 10},
//        {999, 5, 999, 4, 0, 999, 6, 10, 999, 999},
//        {999, 999, 3, 4, 999, 0, 1, 999, 8, 8},
//        {999, 999, 999, 7, 6, 1, 0, 3, 2, 2},
//        {999, 999, 999, 999, 10, 999, 3, 0, 10, 999},
//        {999, 999, 999, 9, 999, 8, 2, 10, 0, 9},
//        {1, 5, 7, 10, 999, 8, 2, 999, 9, 0}
//        };


//    private int[][] selection(int[][] generation) {
//        int[][] newGeneration = new int[chromosomes][gens];
//        for (int i = 0; i < chromosomes/2; i++) {
//            newGeneration[i] = cloneArray(generation, i);
//        }
//        System.out.println("Новое поколение");
//        printGeneration(newGeneration);
//        return newGeneration;
//    }