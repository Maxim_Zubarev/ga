import java.util.*;

public class StartProject {

    public static void main(String[] args){
        Graph graph;
        int size;
        Scanner in = new Scanner(System.in);

        System.out.println("Введите размер графа:");
        size = in.nextInt();

        System.out.println("Введите кол-во поколений:");
        int pok = in.nextInt();

        System.out.println("Введите кол-во хромосом в поколение:");
        int chromosomes = in.nextInt();

        in.close();
        graph = new Graph(size, 1, size, pok, chromosomes);
//        graph = new Graph(10, 1, 10, 30, 8);
        graph.search();
    }
}

