import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Graph {
    private final Random random = new Random();
    private int[][] graph; // начальный граф
    private int[][] generation; // набор хромосом
    private int chromosomes; // кол-во хромосом
    private int size, gens, start, finish, countGeneration;
    private int chance = 10;

    Graph(int size, int start, int finish, int countGeneration, int chromosomes) {
        this.graph = new int[size][size];
        this.size = size;
        this.gens = size + 1;
        this.start = start - 1;
        this.finish = finish - 1;
        this.countGeneration = countGeneration;
        this.chromosomes = chromosomes;
    }

    void search(){
        initGraph();
        printGraph();
        generation = initGeneration(gens, chromosomes);
        getAmount(generation);
        sort(generation);
        int[][] newGeneration = selection(generation);

        for (int i = 2; i < countGeneration; i++) {
            System.out.println(i + " поколение");

            if (i != 2)
                selection(newGeneration);

            crossingOver(newGeneration);
            mutation(newGeneration);
            getAmount(newGeneration);
            sort(newGeneration);
        }

    }

    private void initGraph(){
        for (int i = 0; i < size; i++){
            for (int j = 0; j < size; j++){
                if (i == j){
                    graph[i][j] = 0;
                } else {
                    int rand_number = random.nextInt(100) + 1;
                    graph[i][j] = rand_number;
                    graph[j][i] = rand_number;
                }
            }
        }
    }

    public void printGraph(){
        System.out.print("\t");
        for (int n = 1; n < size + 1; n++){
            System.out.print(n + "\t");
        }
        for (int i = 1; i < size + 1; i++){
            System.out.print("\n");
            System.out.print(i + "\t");
            for (int j = 0; j < size; j++){
                System.out.print(graph[i-1][j] + "\t");
            }
        }
        System.out.println();
    }

    private void printGeneration(int[][] generation){
        for (int i = 0; i < generation.length; i++) {
            for (int j = 0; j < generation[i].length; j++) {
                System.out.print(generation[i][j] + "\t");
            }
            System.out.println();
        }
    }

    private int[][] initGeneration(int gens, int chromosomes){
        generation = new int[chromosomes][gens];

        for(int i = 0; i < chromosomes; i++){
            generation[i][0] = start;
            generation[i][gens-2] = finish;
        }

        for(int i = 0; i < chromosomes; i++){
            for(int j = 1; j < gens - 2; j++){
                generation[i][j] = random.nextInt(gens - 2);
            }
        }
        System.out.println("1 поколение");
        printGeneration(generation);
        return generation;
    }

    private int[][] getAmount(int[][] generation){
        int sum;
        for (int i = 0; i < chromosomes; i++) {
            sum = 0;
            for (int j = 0; j < gens - 2; j++) {
                sum += graph[generation[i][j]][generation[i][j+1]];
            }
            generation[i][gens-1] = sum;
        }
        System.out.println("Сумма");
        printGeneration(generation);
        return generation;
    }

    private int[][] selection(int[][] generation) {
        int[][] newGeneration = new int[chromosomes][gens];
        int[] arr = new int[chromosomes];
        for (int i = 0; i < chromosomes; i++) {
            arr[i] = i;
        }
        Shuffler.shuffle(arr);
        System.out.println("Пары, котороые будут сравниваться");
        System.out.println(Arrays.toString(arr));

        int k = 0;
        for (int i = 0; i < arr.length - 1; i += 2) {
            if (generation[arr[i]][gens-1] < generation[arr[i+1]][gens-1]){
                newGeneration[k] = generation[arr[i]];
            } else {
                newGeneration[k] = generation[arr[i+1]];
            }
            k++;
        }
        System.out.println("Новое поколение");
        printGeneration(newGeneration);
        return newGeneration;
    }

    private int[][] sort(int[][] generation){
        for (int i = 0; i < generation.length-1; i++) {
            for (int j = 0; j < generation.length-i-1; j++) {
                if (generation[j][gens-1] > generation[j+1][gens-1]){
                    int[] temp = generation[j];
                    generation[j] = generation[j+1];
                    generation[j+1] = temp;
                }
            }
        }
        System.out.println("Сортировка");
        printGeneration(generation);
        return generation;
    }

    private int[][] crossingOver(int[][] generation){
        int num;
        int[] arr;
        int[] arr2;
        for (int i = 0; i < chromosomes/2; i += 2) {
            arr = cloneArray(generation, i);
            arr2 = cloneArray(generation, i+1);

            for (int j = 1; j < (gens-2)/2; j++) {
                num = arr[j];
                arr[j] = arr2[j];
                arr2[j] = num;
            }
            generation[i+chromosomes/2] = arr;
            generation[i+1+chromosomes/2] = arr2;
        }
        System.out.println("Скрещивание");
        printGeneration(generation);
        return generation;
    }

    private int[][] mutation(int[][] newGeneration){
        for (int i = chromosomes/2; i < chromosomes; i++) {
            if (random.nextInt(100) < chance){
                int amount = random.nextInt(gens-4) + 1; // опеределение кол-ва генов, которые будут мутировать
                for (int j = 0; j < amount; j++) {
                    int randPosition = random.nextInt(gens-4) + 1;
                    newGeneration[i][randPosition] = newGeneration[i][randPosition+1];
                }
            }
        }
        System.out.println("Мутация");
        printGeneration(newGeneration);
        return newGeneration;
    }

    private int[] cloneArray(int[][] baseArray, int pos){
        int[] arr = new int[gens];
        if (baseArray[pos].length >= 0) System.arraycopy(baseArray[pos], 0, arr, 0, baseArray[pos].length);
        return arr;
    }
}
